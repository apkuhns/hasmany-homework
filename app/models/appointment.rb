class Appointment < ActiveRecord::Base
  belongs_to :patient
  belongs_to :specialist

  validates_presence_of :fee
  validates_numericality_of :fee, greater_than_or_equal_to: 0


  def days_until_appointment
    (appointment_date - Date.today).to_int
  end
end
