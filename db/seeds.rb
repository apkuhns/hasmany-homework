# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Insurance.delete_all
Patient.delete_all
Specialist.delete_all
Appointment.delete_all

#Create the insurance companies
insurance_companies = []
%w{ Abcd BigBad Expensive BlueShield Regressive}.each do |ins|
  insurance_companies << Insurance.create(name: ins, street_address: "#{rand(10000)} #{ins} Street")
end

#Create patients, and associate with insurance companies
%w{Fred Ethel Lucy Ricky Voldemort}.each_with_index do |name,index|
  Patient.create(name: name, insurance: insurance_companies[index], street_address: "1313 Mockingbird Lane")
end

#Create specialists
specialist_hash = {"Dr. Watson" => "Investigation", "Dr. Doolittle" => "Horseness", "Dr. Doctor" => "Internal Medicine",
                   "Dr. Pain" => "Dentist", "Dr. Who" => "Time Travel"}

specialists = []
specialist_hash.keys.each do |key|
  specialists << Specialist.create(name: key, speciality: specialist_hash[key])
end

#Create appointments. For demonstration purposes, the appointments are set at random
#times in the future
patients = Patient.all
patients.each do |patient|

  n = rand(5)
  sp1 = specialists[n]
  sp2 = specialists[(n + 1) % 5]

  app1 = Appointment.create(patient: patient, specialist:sp1, appointment_date: Date.today + rand(20))
  app2 = Appointment.create(patient: patient, specialist:sp2, appointment_date: Date.today + rand(20))
end

